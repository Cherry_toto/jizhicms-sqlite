<?php return array (
  'db' => 
  array (
    'host' => '127.0.0.1',
    'dbname' => '',
    'username' => '',
    'password' => '',
    'prefix' => 'jz_',
    'port' => '3306',
    'type' => 'mysql',
    'dbpath' => '',
  ),
  'redis' => 
  array (
    'SAVE_HANDLE' => 'Redis',
    'HOST' => '127.0.0.1',
    'PORT' => 6379,
    'AUTH' => NULL,
    'TIMEOUT' => 0,
    'RESERVED' => NULL,
    'RETRY_INTERVAL' => 100,
    'RECONNECT' => false,
    'EXPIRE' => 1800,
  ),
  'APP_DEBUG' => true,
); ?>